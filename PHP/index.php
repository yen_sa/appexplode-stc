<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
        integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">

    <title>AppExplode</title>
    <link rel="shortcut icon" href="img/logo.png">
</head>

<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-white text-dark sticky-top">
        <a class="navbar-brand font-weight-bold" href="#"><img src="/img/logo.png" width="37" alt=""
                style="margin: 5px; margin-left:50px;">AppExplode</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNavbar"
            aria-controls="myNavbar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="navbar-nav ml-md-auto text-dark">
                <li nav-item><a class="nav-link m-2 mx-3 nav-active" href="">Home</a></li>
                <li nav-item><a class="nav-link m-2 mx-3" href="#">About Us</a></li>
                <li nav-item><a class="nav-link m-2 mx-3" href="#">Portfolio</a></li>
                <li nav-item><a class="nav-link m-2 mx-3" href="#">Approach</a></li>
                <li nav-item><a class="nav-link m-2 mx-3" href="#">Team</a></li>
                <li nav-item><a class="nav-link m-2 mx-3" href="#">Contact Us</a></li>
            </ul>
        </div>
        <div class="navbar-instant">
            <button type="button" class="btn btn-warning text-white mx-5"
                style="background-color: blueviolet; border: blueviolet;">Instant Quote</button>
        </div>
    </nav>
    <!-- banner -->
    <header>
        <div class="banner container-fluid text-white">
            <div class="row" style="padding: 75px; margin: 0;">
                <div class="col-sm-6 m-auto text-uppercase text-center">
                    <h1 class="font-weight-bold" style="font-size: 120px;">Build your dreams</h1>
                </div>
                <div class="col-sm-6 m-auto" style="padding: 100px; font-size: 29px;">
                    <p>The App Explode ethos is to bring digital dreams to reality.If you heve a vision for an app or a
                        digital product (apps, website, games), we are the team to build it.</p>
                </div>
            </div>
        </div>
    </header>
    <!-- end of banner -->

    <!-- who we are -->
    <div class="header-who">
        <div class="row" style="margin: 0;">
            <div class="col m-5" style="padding-left: 50px;">
                <h1 class="font-weight-bold" style="font-size:100px;">Who</h1>
                <h1 class="font-weight-bold" style="font-size:80px; margin-top:-50px;">we are</h1>
            </div>
            <div class="col m-5" style="font-size: 30px; padding-right: 80px;">
                <p>App Explode specialises in helping ideas come to life quick. We help you finesse your idea, work
                    through designs, and produce a rapid phototype in as little as 3 weeks.</p>
            </div>
        </div>
    </div>
    <div class="row" style="margin: 0;">
        <div class="col m-5" style="padding-left: 50px; font-size: 30px;">
            <b> Turn your app idea into reality, FAST</b>.<br>Build it and
            they will come is a myth. We work with entrepreneurs and innocators to bring their app ideas to life and
            help them find their first users with proven early adopter customer acquisition strategies.</br>
            <button type="button" class="btn btn-warning text-white my-5 text-uppercase"
                style="background-color: blueviolet; border: blueviolet; font-size: 20px;">Instant Quote</button>
        </div>
        <div class="col" style="padding: 0;">
            <img src="img/phone.jpg" alt="" width="755.8">
        </div>
    </div>
    <!-- end of who we are -->

    <!-- our process -->
    <div class="header-our bg-dark text-white">
        <div class="content-our m-5 p-5">
            <h1 class=" font-weight-bold" style="font-size: 50px;">Our Process</h1>
        </div>
        <div class="row" style="margin-left: 200px; margin-right: 200px;">
            <div class="col" style="margin-right: 100px;">
                <h2 class="text-center">1.</h2>
                <h2>Chat With an App Expert</h2>
                <p style="font-size: 17px;">Speak with one of our friendly app experts. We're happy to sign an NDA! We
                    work together using
                    our app quote form to define key features of your app and bring the idea to life. Book in a time
                    here.</p>
            </div>
            <div class="col" style="margin-left: 100px;">
                <h2 class="text-center">2.</h2>
                <h2>Your App Quote in 48 Hours</h2>
                <p style="font-size: 17px;">With the information collected in the first step, the App Explode team will
                    plan out all the design
                    and development requirements for your app, and deliver your quote fast. Prices range from $10,000 -
                    $150,000.</p>
            </div>
        </div>
        <div class="row" style="margin-left: 200px; margin-right: 200px; margin-top: 50px;">
            <div class="col" style="margin-right: 100px;">
                <h3 class="text-center">4.</h3>
                <h3>Start With Desgin</h3>
                <p style="font-size: 17px;">Once the project is agreed, design begins! Planning and design starts prior
                    to development, and
                    AppExplode apps are always a thing of beauty.We also do a kickoff meeting to</p>
            </div>
            <div class="col" style="margin-left: 100px;">
                <h3 class="text-center">3.</h3>
                <h3>Coding Begins</h3>
                <p style="font-size: 17px;">Development of your app begins after just a week of design work, and
                    continues on concurrenetly white
                    the rest of the screen designs are finished. We work with React</p>
            </div>
        </div>
        <div class="row" style="margin-left: 200px; margin-right: 200px; margin-top: 50px; padding-bottom: 50px;">
            <div class="col" style="margin-right: 100px;">
                <h3 class="text-center">5.</h3>
                <h3>App Launch in 12 Weeks!</h3>
                <p style="font-size: 17px;">We work hanrd to get your app to market within 12 Weeks. This requires
                    focus, planning and concurrent
                    design and development. Some apps might require more time, but speed to market is our goal.</p>
            </div>
            <div class="col" style="margin-left: 100px;">
                <h3 class="text-center">6.</h3>
                <h3>Support & Marketing</h3>
                <p style="font-size: 17px;">Once your app is built, the next phase of your journey strategy and
                    partnerships, and support you any further development requirements. Includes 60 day bug fixing.</p>
            </div>
        </div>
    </div>
    <!-- end of our process -->

    <!-- what our client's say -->
    <div class="header-what">
        <div class="content-what m-5 pl-5 pt-5">
            <h1 class=" font-weight-bold" style="font-size: 50px;">What our</h1>
            <h1 class=" font-weight-bold" style="font-size: 50px; margin-bottom: 10px;">Client's say</h1>
        </div>
        <!-- <img src="img/people.jpg" alt="" class="what-img mx-auto d-block"> -->
        <div id="demo" class="carousel slide" data-ride="carousel">

            <!-- The slideshow -->
            <div class="carousel-inner text-center">
                <div class="carousel-item active">
                    <img src="img/img1-01.jpg" alt="" width="700px">
                </div>
                <div class="carousel-item">
                    <img src="img/img2-01.jpg" alt="" width="700px">
                </div>
                <div class="carousel-item">
                    <img src="img/img3-01.jpg" alt="" width="700px">
                </div>
                <div class="carousel-item">
                    <img src="img/img4-01.jpg" alt="" width="700px">
                </div>
            </div>

            <!-- Left and right controls -->

            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <img src="img/back.png" class="carousel-control-prev-icon float-left ml-5" style="margin-top: 750px;"
                    alt="" width="30px">
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
                <img src="img/next.png" class="carousel-control-next-icon float-right mr-5" style="margin-top: 750px;"
                    alt="" width="30px">
            </a>
        </div>
        <div class="text-center" style="font-size: 20px;">
            <img src="img/nhay-01-01.jpg" alt="" width="100">
            <p class="mt-5">As a valued partner that understands the needs of startups, I cant recommend app explode
                enough.</p>
            <!-- <img src="img/back.svg" class="float-left ml-5" alt="" width="30px">
            <img src="img/next.svg" class="float-right mr-5" alt="" width="30px"> -->
            <p class="text-uppercase font-weight-bold" style="margin-top: 80px; margin-bottom: 0px;">patrick anderrson
            </p>
            <p class="mb-5 pb-5">Founder-Dressium</p>
        </div>
    </div>
    <!-- end of what our client's say -->

    <!-- what we do -->
    <div class="header-whatwedo bg-light" style="padding-bottom: 70px;">
        <div class="row" style="margin: 0;">
            <div class="col m-5">
                <div style=" padding-top: 50px; padding-left: 40px;">
                    <h1 class=" font-weight-bold" style="font-size: 50px;">What We Do</h1>
                </div>
            </div>
            <div class="col m-5" style="font-size: 20px; padding-right: 80px; padding-top: 50px;">
                <p>App Explore's experence can be the difference between struggling and accelerated growth. If you want
                    your user experience to be top-notch and your business impact at its best, we're the people who can
                    help you get there.</p>
            </div>
        </div>
        <!-- app and web -->
        <!-- <div class="row"> -->
        <!-- app -->
        <!-- <div class="col">
                <div class="row bg-white"
                    style="border-radius: 10px; padding-right: 20px; padding-left: 20px; padding-top: 40px; padding-bottom: 40px; margin-left: 80px; margin-right: 10px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                    <div class="col-sm-4">
                        <img src="img/icon1.jpg" alt="">
                    </div>
                    <div class="col-sm-8 text left">
                        <h2>App</h2>
                        <p>Get hands-on guidance in positioning your product in the market.</p>
                    </div>
                </div>
            </div> -->
        <!-- end of app -->

        <!-- website -->
        <!-- <div class="col">
                <div class="row bg-white"
                    style="border-radius: 10px; padding-right: 20px; padding-left: 20px; padding-top: 40px; padding-bottom: 40px; margin-right: 80px; margin-left: 10px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                    <div class="col-sm-4">
                        <img src="img/icon2.jpg" alt="">
                    </div>
                    <div class="col-sm-8">
                        <h2>Websites</h2>
                        <p>Discover the power of research in improving your buisiness in leaps and bounds.</p>
                    </div>
                </div>
            </div> -->
        <!-- end of web -->
        <!-- end of app and web -->

        <!-- game and ux/ui -->
        <!-- <div class="row mt-5" style="padding-bottom: 80px;"> -->
        <!-- game -->
        <!-- <div class="col">
                <div class="row bg-white"
                    style="border-radius: 10px; padding-right: 20px; padding-left: 20px; padding-top: 40px; padding-bottom: 40px; margin-left: 80px; margin-right: 10px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                    <div class="col-sm-4">
                        <img src="img/icon3.jpg" alt="">
                    </div>
                    <div class="col-sm-8">
                        <h2>Game</h2>
                        <p>Find new angles to improve your product with well-planned, cost-effective user experience
                            changes.</p>
                    </div>
                </div>
            </div> -->
        <!-- end of game -->

        <!-- ux/ui -->
        <!-- <div class="col">
                <div class="row bg-white"
                    style="border-radius: 10px; padding-right: 20px; padding-left: 20px; padding-top: 40px; padding-bottom: 40px; margin-right: 80px; margin-left: 10px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                    <div class="col-sm-4">
                        <img src="img/icon4.jpg" alt="">
                    </div>
                    <div class="col-sm-8">
                        <h2>UX/UI</h2>
                        <p>Stop focusing on putting out fires and map out your growth to bring your product at the
                            next
                            level.</p>
                    </div>
                </div>
            </div> -->
        <!-- end of ux/ui -->
        <div class="row m-auto">
            <div class="col m-3">
                <div class="what-app bg-white p-5" style="border-radius: 10px; margin-left: 70px;">
                    <img src="img/icon1.jpg" alt="" class="float-left pr-4">
                    <h2>App</h2>
                    <p>Get hands-on guidance in positioning your product in the market.</p>
                </div>
            </div>
            <div class="col m-3">
                <div class="what-web bg-white p-5" style="border-radius: 10px; margin-right: 70px;">
                    <img src="img/icon2.jpg" alt="" class="float-left pr-4">
                    <h2>Websites</h2>
                    <p>Discover the power of research in improving your buisiness in leaps and bounds.</p>
                </div>
            </div>
        </div>

        <div class="row m-auto">
            <div class="col m-3">
                <div class="what-game bg-white p-5" style="border-radius: 10px; margin-left: 70px;">
                    <img src="img/icon3.jpg" alt="" class="float-left pr-4">
                    <h2>Game</h2>
                    <p>Find new angles to improve your product with well-planned, cost-effective user experience
                        changes.</p>
                </div>
            </div>
            <div class="col m-3">
                <div class="what-uxui bg-white p-5" style="border-radius: 10px; margin-right: 70px;">
                    <img src="img/icon4.jpg" alt="" class="float-left pr-4">
                    <h2>UX/UI</h2>
                    <p>Stop focusing on putting out fires and map out your growth to bring your product at the next
                        level.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- end of game and ux/ui -->
    </div>
    <!-- end of what we do -->

    <!-- get it -->
    <div class="header-getit">
        <div class="row m-5">
            <div class="col-sm-6">
                <div style="padding-top: 50px; padding-left: 40px;">
                    <h1 class=" font-weight-bold" style="font-size: 50px;">Get in touch with the team behind the apps
                    </h1>
                </div>
            </div>
            <div class="col-sm-4 text-uppercase" style="font-size: 20px; padding-top: 50px; margin-left: 200px;">
                <p style="font-size: 15px; margin-bottom: 5px;">Phone</p>
                <h4 class="font-weight-bold">+84 234 385 7677</h4>
            </div>
        </div>

        <div class="row m-5">
            <div class="col-sm-6">
                <div style="padding-left: 40px; margin-right: 220px;">
                    <p style="font-size: 15px;">App Explode is a Sydney based cohort of designers, developers and
                        technologists thay love
                        breathing life intro great ideas. Call us today on 0420 922215 or contact us via the form below.
                    </p>
                </div>
            </div>
            <div class="col-sm-4" style="font-size: 20px; margin-left: 200px;">
                <p class="text-uppercase" style="font-size: 15px; margin-bottom: 5px;">Email</p>
                <h4 class="font-weight-bold">team@appexplode.com</h4>
            </div>
        </div>
        <div class="row text-muted" style="padding-bottom: 300px; margin: 0;">
            <div class="col-sm-5" style="padding-left: 100px;">
                <div class="box">
                    <form action="form.php" method="post">
                        <div>
                            <input type="text" class="form-control" required="" name="name">
                            <label>Your Name*</label>

                        </div>
                        <div>
                            <input type="email" class="form-control" required="" name="email">
                            <label>Your Email*</label>

                        </div>
                        <div>
                            <input type="tel" class="form-control" required="" name="phone">
                            <label>Your Phone Number*</label>

                        </div>
                        <div>
                            <input type="text" class="form-control" required="" name="website">
                            <label>Your website*</label>

                        </div>
                        <div>
                            <textarea class="form-control" name="comment" required=""></textarea>
                            <label>Tell us briefly about your app idea.</label>

                        </div>
                        <button type="submit" name="submit" class="btn btn-primary text-uppercase my-4 py-2 px-4"
                            style="background: blueviolet; border: blueviolet;">Submit</button>
                    </form>
                </div>
            </div>

            <div class="col-sm-6">
                <img src="img/mes.png" alt="" width="750">
            </div>
        </div>
    </div>
    <!-- end of get it -->
    <!-- contact -->
    <div class="header-contact">
        <div class="row" style="margin-left: 115px; margin-right: 50px; padding: 0;">
            <div class="col-sm-3">
                <a class="navbar-brand font-weight-bold text-dark mb-3" href="#"><img src="/img/logo.png" width="37"
                        alt="">AppExplode</a>
                <div style=" font-size: 15px;">
                    <P class="font-weight-bold">Business Hours</P>
                    <p>Monday - Friday 08:00AM to 05:00PM</p>
                    <p style="margin-top: -10px;">Saturday - Closed</p>
                    <p style="margin-top: -10px;">Sunday - Closed</p>
                </div>
                <div class="icons-brand">
                    <i class="fab fa-facebook-square mr-3" title="Facebook"></i>
                    <i class="fab fa-instagram mr-3" title="Instagram"></i>
                    <i class="fab fa-twitter mr-3" title="Twitter"></i>
                </div>
            </div>
            <div class="col-sm-3">
                <a class="navbar-brand font-weight-bold text-dark mb-3 ">Contact</a>
                <div class="content-contact">
                    <i class="fas fa-envelope mb-3" style="margin-top: 45px;"><span class="ml-3 font-weight-normal">team@appexplode.com</span></i>
                    <i class="fas fa-phone mb-3"><span class="ml-3 font-weight-normal">LLOYD +61 40 444 1243</span></i>
                    <i class="fas fa-phone"><span class="ml-3 font-weight-normal">PAM +61 420 922 215</span></i>
                </div>
            </div>
            <div class="col-sm-3">
                <a class="navbar-brand font-weight-bold text-dark mb-3">Company</a>
                <p style="margin-top: 40px;">Who we are</p>
                <p>How it work</p>
                <p>Team</p>
                <p>Instant Quote</p>
            </div>
            <div class="col-sm-3">
                <a class="navbar-brand font-weight-bold text-dark mb-3">Help</a>
                <p style="margin-top: 40px;">Terms and Conditions</p>
                <p>Privacy Policy</p>
            </div>
        </div>
    </div>
    <p style="margin-left: 130px; margin-top: 10px;">Appexplode, 2019, All rights reserved.</p>
    <!-- end of contact -->
    <div class="row" style="padding: 0; margin: 0;">
        <div class="col">
            <p class="font-italic" style="margin-left: 115px; margin-top: 30px;">*We dont's share your personal info or
                idea with anyone. We are happy to sign on NDA before full of an idea. Check
                out our Privacy for more infomation.</p>
        </div>
        <div class="col">

        </div>
    </div>

</body>

</html>